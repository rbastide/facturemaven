/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facture;

import java.io.PrintStream;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author rbastide
 */
public class Facture {
    
    private final Client destinataire;
    private final Date emission;
    private final int numero ;
    // déclaration de la variable : on utilise une interface (List)
    private List<LigneFacture> articlesFactures;

    public Facture(Client destinataire, Date emission, int numero) {
        this.destinataire = destinataire;
        this.emission = emission;
        this.numero = numero;
       // initialisation de la variable : on choisit une implémentation (LinkedList)
        articlesFactures = new LinkedList<>();      
    }

    /**
     * Get the value of numero
     *
     * @return the value of numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Get the value of emission
     *
     * @return the value of emission
     */
    public Date getEmission() {
        return emission;
    }

    /**
     * Get the value of destinataire
     *
     * @return the value of destinataire
     */
    public Client getDestinataire() {
        return destinataire;
    }


    
    public void ajouteLigne(Article a, int nombre, float remise) {
        LigneFacture lf = new LigneFacture(nombre,this, a, remise);
        articlesFactures.add(lf);

        
   }
    
   public float montantTTC(float tauxTVA) {
       // 1° ligne : je déclare le résultat;
       float result = 0f;
       
       // je calcule le résultat
       for(LigneFacture ligne : articlesFactures) {
           result += ligne.montantLigne();
       }
       // J'applique le taux de TVA
       result *= (1 + tauxTVA);
       
       // dernière ligne : je renvoie le résultat
       return result;
   }
   
   public void print(PrintStream out, float tva) {
       // imprimer l'en-tête de la facture
       out.printf("Facture n° %d du %s \n", numero, emission);
       out.printf("Client : %s\n", destinataire.getName());
       out.printf("Adresse : %s\n", destinataire.getAddress());
       out.println("--- Articles facturés ---");
       for (LigneFacture ligne : articlesFactures)
           out.printf("%s\n", ligne); 
       out.println("-------------------");
       out.printf("Le montant TTC est %6.2f€ \n", montantTTC(tva)); // 1380.0
           
   }
}
